" File settings for gitcommit filetype

setlocal spell
setlocal textwidth=72
setlocal tabstop=2 shiftwidth=2 softtabstop=2 cindent
