" Run program with argument(s) directly from Vim
func! CompileRun#RunWithArgs#RunWithArgs()
    silent !echo -e "\n\033[32;1m* * * * Enter argument(s) * * * *\033[0;m"
    !xargs -L 1 ./%<
endf
