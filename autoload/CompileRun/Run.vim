" Run program (without any argument) directly from Vim
func! CompileRun#Run#Run()
    silent !echo -e "\n\033[32;1m* * * * * * * * * Run * * * * * * * *\033[0;m"
    write
    if &filetype == "bash" || &filetype == "sh"
        !bash %
    elseif &filetype == "c" || &filetype == "cpp"
        !./%<
    elseif &filetype == "java"
        !java %<
    elseif &filetype == "python"
        if g:python == 3
            !python3 %
        else
            !python %
        endif
    elseif &filetype == "tex"
        !xdg-open %<.pdf 2>/dev/null &
    else
        echom "Cannot run file of type " . &filetype
    endif
endf
