" Compile program directly from Vim
func! CompileRun#Compile#Compile()
    silent !echo -e "\n\033[31;1m* * * * * * Compilation  * * * * * *\033[0;m"
    write
    if &filetype == "c" || &filetype == "cpp"
        silent !echo
        silent make! %:r
        if len(getqflist()) > 1
            :!
        endif
    elseif &filetype == "java"
        !javac %
    elseif &filetype == "make"
        !make
    elseif &filetype == "tex"
        !pdflatex %
    else
        echom "Cannot compile file of type " . &filetype
    endif

    redraw!
endf
