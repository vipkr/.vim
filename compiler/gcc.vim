if exists("current_compiler")
  finish
endif
let current_compiler = "gcc"

let $CFLAGS = "-O2 -std=c11 -pedantic -Wall -Werror -Wextra"
let $CXXFLAGS = "-O2 -std=c++14 -Wall -Wextra -pedantic"
let $CXXFLAGS .= " -Wformat=2 -Wfloat-equal -Wlogical-op -Wredundant-decls -Wconversion -Wcast-qual -Wcast-align -Wuseless-cast -Wno-shadow -Wno-unused-result -Wno-unused-parameter -Wno-unused-local-typedefs -Wno-long-long"
let $CXXFLAGS .= " -g -fsanitize=address,undefined"
