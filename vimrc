" ===========================  Table of contents  ==========================
"   Select TOC_XX and press * to go to the corresponding section
func! TableOfContent()

    TOC_01 Plugins
    TOC_02 Vundle Stuff
    TOC_03 General Settings
        TOC_03_01 UI Settings
    TOC_04 Compiler Settings
    TOC_05 File Settings
    TOC_06 Mappings
        TOC_06_01 Function Mappings
    TOC_07 Helper Commands
endf



" ============================== Plugins ( TOC_01 ) =========================
packadd! matchit

" ===========================================================================



" ===========================  Vundle stuff ( TOC_02 ) ======================
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
" call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo


" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
" filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" ============================================================================



" ========================= General Settings ( TOC_03 )  =====================
set encoding=utf-8
set fileencoding=utf-8
set ffs=unix,dos,mac        " use Unix as the standard file type

set timeoutlen=700
syntax on
let g:c_space_errors = 1
set list
set listchars=tab:>-
" <C-A> works on 07
set nrformats-=octal
" no new tab after namespace
set cinoptions+=N-sl1
set conceallevel=1
set shortmess+=c
set langremap
let &tags.=",tags;"

" Sets how many lines of history VIM has to remember
set history=500

" Set to auto read when a file is changed from the outside
set autoread

set undofile
set undolevels=1000
set directory=~/.vim/.swp//
set undodir=~/.vim/.undo//


" ==========================  UI Settings ( TOC_03_01 )  ======================
" Turn on the Wild menu
" set wildmenu

set number          " Show line numbers
set linebreak       " Break lines at word ( requires wrap lines )
set showbreak=+++   " Wrap-broken line prefix
" set textwidth=80  " Line wrap (number of cols) (currently: don't like line break behavior)
set showmatch       " Highlight matching brace
set mat=2           " No. of tenths of a second to blink when matching brackets
" set spell         " Enable spell-checking
set visualbell      " Use visual bell (no beeping)

set hlsearch        " Highlighting all search results
set smartcase       " Enable smart-case search
set ignorecase      " Always case-insensitive
set incsearch       " Searches for strings incrementally

" set autoindent
" set cindent       " Use 'C' style program indenting
" set smartindent   " Enable smart indent
set expandtab       " Use spaces instead of tabs
set smarttab        " Enable smart-tabs
set shiftwidth=4    " Number of columns with re-indent operator (>>/<<)
set tabstop=4       " Number columns a tab counts for
set softtabstop=4   " Number of columns when hit tab in Insert mode

" Advance
set ruler           " Always show current position
set cmdheight=1     " Height of the command bar
set hid             " A buffer becomes hidden when it is abandoned
set backspace=eol,start,indent  " Configure backspace so it acts as it should act
set whichwrap+=h,l
set showcmd
set matchpairs+=<:>
set mouse=a
set ttymouse=xterm2
" set term=xterm
set complete-=i
set wildmenu
set splitright
set splitbelow
set lazyredraw          " Don't redraw while executing macros (good performance config)
set foldcolumn=1        " Add a bit extra margin to the left
set background=dark

" ============================================================================



" =======================  Compiler Settings ( TOC_04 ) ======================
runtime /compiler/*.vim
let g:python=3

" ============================================================================



" =========================  File Settings ( TOC_05 ) ========================
" Return to last edit position when opening files
autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" Check CleanExtraSpaces helper function in 'autoload/CleanExtraSpaces.vim'
autocmd BufWritePre *.txt,*.py,*.sh :call CleanExtraSpaces()

" ============================================================================



" ===========================  Mappings ( TOC_06 )  ==========================
" Change mapleader to , (\ is default)
let mapleader = ","

" Fast saving
nmap <Leader>w :w!<Enter>

" Allow saving saving of files as sudo
command W w !sudo tee %  > /dev/null

" Copy current file to X clipboard
map <F4> :!xclip -selection clipboard < %<Enter><Enter>

" Copy and paste
noremap <Leader>y "*y
noremap <Leader>p "*]p
noremap <Leader>Y "+y
noremap <Leader>P "+]p

" Remove the Windows ^M - when the encodings gets messed up
noremap <Leader>m mmHmt:%s/<C-V><Enter>//ge<Enter>'tzt'm

" Quickly open a buffer for scribble
map <Leader>txt :e ~/tmp/buffer.txt<Enter>

" Quickly open a markdown buffer for scribble
map <Leader>md :e ~/tmp/buffer.md<Enter>

" =====================  Function Mappings ( TOC_06_01 )  ====================
" Functions are defined in 'autoload/CompileRun/' folder
map <F9> :call CompileRun#Compile#Compile()<Enter>
imap <F9> <Esc>:call CompileRun#Compile#Compile()<Enter>
map <F5> :call CompileRun#Run#Run()<Enter>
imap <F5> <Esc>:call CompileRun#Run#Run()<Enter>
map <F6> :call CompileRun#RunWithArgs#RunWithArgs()<Enter>
imap <F6> <Esc>:call CompileRun#RunWithArgs#RunWithArgs()<Enter>

" ============================================================================



" ======================= Helper Commands ( TOC_07 )  ========================
command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis | wincmd p | diffthis

" ============================================================================
